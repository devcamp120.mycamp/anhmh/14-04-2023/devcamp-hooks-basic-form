import React, { useEffect, useState } from 'react'

function UserForm() {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
    const [lastName, setLastName] = useState(localStorage.getItem("lastName"));
    
    const handleFirstNameChange = (event) => {
        console.log(event.target.value);
        setFirstName(event.target.value)
    }
    const handleLasttNameChange = (event) => {
        console.log(event.target.value);
        setLastName(event.target.value)
    }
    useEffect(() => {
        console.log("componentDidUpdate!");
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
    })

    return (
        <>
            <div>
                <input value={firstName} onChange={handleFirstNameChange} placeholder="devcamp" />
            </div>
            <div>
                <input value={lastName} onChange={handleLasttNameChange} placeholder="user" />
            </div>
            <p>{firstName} {lastName}</p>
        </>
    )
}

export default UserForm
